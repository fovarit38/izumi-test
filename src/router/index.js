import {
    Routes,
    Route
} from "react-router-dom";


import MainScreen from '../views/MainScreen';
import CodeScreen from '../views/CodeScreen';


const Router = (() => {
    return (
        <Routes>
            <Route path="/" element={<MainScreen/>}/>
            <Route path="/code" element={<CodeScreen/>}/>
        </Routes>
    )
})
export default Router;
