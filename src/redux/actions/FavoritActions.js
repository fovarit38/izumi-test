import {store} from '../index';

export const addFavorit = (res) => {
    store.dispatch({
        type: 'ADD_FAVORITE',
        payload: res
    });
}


export const deleteFavorit = (res) => {
    store.dispatch({
        type: 'DELETE_FAVORITE',
        payload: res
    });
}
