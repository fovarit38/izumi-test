import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

import {getAnime} from "../../services";
import favoritesSlices from "../../redux/slices/favorites";

import './style.scss'
import CloseIcon from '../../assets/images/close.png';

import Layout from "../../components/Layout";
import Article from "../../components/Article";
import More from "../../components/More";
import Loader from "../../components/Loader";


const MainScreen = (() => {

    //списко id в формате [id,id,id,...]
    const {favorite} = useSelector(favoritesSlices);

    const [page, setPage] = useState(1);

    const [loading, setLoading] = useState('none');

    //тут храним поиск
    const [search, setSearch] = useState('');

    //ресеттер, для актуализации данных
    const [reset, setReset] = useState(false);

    //тут храним id таймера, чтобы запрос в DB отправлялся раз в 600 мс
    const [timer, setTimer] = useState(0);

    //Хранение для обычных аниме
    const [animeList, setAnimeList] = useState([]);

    //Хранение для избранных аниме
    const [favoritLocalDetails, setFavoritLocalDetails] = useState([]);

    // Тут мы просто ищем аниме
    const getAnimeApi = async () => {
        setLoading('send');
        setAnimeList((await getAnime(
            {
                search,
                page: 1,
                sort: "SEARCH_MATCH",
                type: "ANIME",
                perPage: 3
            }
        )));
        setLoading('done');
    }

    const moreEvent = async (event) => {
        const currentPage = page + 1;
        setPage(currentPage);
        setLoading('more');

        const animeMore = (await getAnime(
            {
                search,
                page: currentPage,
                sort: "SEARCH_MATCH",
                type: "ANIME",
                perPage: 3
            }
        ));

        setAnimeList({
            ...animeMore, ...{
                media: [...animeList.media, ...animeMore.media]
            }
        });
        setLoading('done');
    }

    // Тут мы подтягиваем актуальную информацию по id анимешек
    const getAnimeFavoritesDetailsApi = async () => {
        setFavoritLocalDetails(await getAnime({
            id_in: favorite,
        }));
    }

    const searchEvent = (event) => {
        setSearch(event.target.value);
        if (timer) {
            clearTimeout(timer);
            setTimer(0);
        }
        setTimer(setTimeout(() => {
            setPage(1);
            setReset(!reset);
            setTimer(0);
        }, 600));
    }


    useEffect(() => {
        if (search.length > 0) {
            getAnimeApi();
        }
    }, [reset]);

    useEffect(() => {
        if (favorite.length > 0) {
            getAnimeFavoritesDetailsApi();
        } else {
            setFavoritLocalDetails([]);
        }
    }, [favorite.toString()]);


    return (
        <Layout>
            <div className={'search'}>
                <p className={'search__title text text-s36 text-green'}>
                    Список аниме
                </p>
                <div className="search__block">
                    <input type="text" value={search} onChange={searchEvent} placeholder={'Text here'}
                           className={'search__input text text-s16'}/>
                    {
                        search && (
                            <button onClick={() => {
                                if (timer) {
                                    clearTimeout(timer);
                                    setTimer(0);
                                }
                                setPage(1);
                                setSearch('');
                                setAnimeList([]);
                            }} className={'search__clear'}>
                                <img className={'search__clear-img'} src={CloseIcon}/>
                            </button>
                        )
                    }
                </div>
            </div>
            <div className="article-block">
                {
                    (loading == 'done' || loading == 'more') && (
                        <div className="article-block__container">
                            {
                                animeList?.media && animeList.media.map((item, index) => {
                                    return (
                                        <Article original={true} key={index.toString()} id={item.id}
                                                 images={item.coverImage.large} desk={item.description}
                                                 title={item.title.userPreferred} titleOriginal={item.title.native}/>
                                    )
                                })
                            }
                        </div>
                    )
                }

                {(loading == 'send' || loading == 'more') && (
                    <Loader/>
                )}

                {(loading == 'done' || loading == 'more') && animeList?.pageInfo?.lastPage && page < animeList?.pageInfo?.lastPage && (
                    <div className={'block-center'}>
                        <More name={'MORE'} onClick={moreEvent}/>
                    </div>
                )}


            </div>

            <div className={'search'}>
                <p className={'search__title text text-s36 text-green'}>
                    Любимое аниме
                </p>
            </div>
            <div className="article-block">
                <div className="article-block__container">
                    {
                        favoritLocalDetails?.media && favoritLocalDetails.media.map((item, index) => {
                            return (
                                <Article className={'article-block__item--image-left'} key={index.toString()} id={item.id}
                                         images={item.coverImage.large} desk={item.description}
                                         title={item.title.userPreferred} titleOriginal={item.title.native}/>
                            )
                        })
                    }
                </div>
            </div>
        </Layout>
    );
});
export default MainScreen;
