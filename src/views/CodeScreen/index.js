import Layout from "../../components/Layout";
import Article from "../../components/Article";
import './style.scss'

import {useLocation} from "react-router-dom";
import {useEffect} from "react";
import {getToken} from "../../services";


const MainScreen = (() => {

    const search = useLocation().search;
    const code = new URLSearchParams(search).get('code');

    useEffect(async () => {
        if (code) {

        }
    }, []);
    return (
        <Layout>
            <div className={'code-block'}>
                {
                    !code && (
                        <p className={'code-block__error text text-s36 text-green'}>
                            мы не получили код для авторизации
                        </p>
                    )
                }
                {
                    code && (
                        <p className={'code-block__ok text text-s16 text-green'}>
                            {code}
                        </p>
                    )
                }
            </div>
        </Layout>
    );
});
export default MainScreen;
