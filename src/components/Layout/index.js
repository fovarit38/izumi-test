import React from 'react';

import {NotificationContainer, NotificationManager} from 'react-notifications';

import Logo from '../../assets/images/logo.png';

const Layout = (({children}) => {


    return (
        <>
            <div className={'main-box'}>
                <NotificationContainer/>
                <header className={'header'}>
                    <img className={'logo'} src={Logo}/>
                </header>
                <main>{children}</main>
            </div>
        </>
    )
});
export default Layout;
