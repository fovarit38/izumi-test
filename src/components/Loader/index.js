import React from 'react';
import './style.scss';


const Loader = (({name, onClick, disable}) => {
    return (
        <>
            <div className={'block-center'}>
                <div className="loadingio-spinner-spinner-zmr4czewhnq">
                    <div className="ldio-49elo8ctmd7">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </>
    )
});


export default Loader;
