import React from 'react';
import {connect, useSelector} from 'react-redux';

import favoritesSlices from '../../redux/slices/favorites';

import {addFavorit, deleteFavorit} from '../../redux/actions/FavoritActions';

import './style.scss';

import favoritIcon from '../../assets/images/favorit.png';
import closeIcon from '../../assets/images/close.png';

const Article = (({title, className, images, id, titleOriginal, original = false}) => {
    const {favorite} = useSelector(favoritesSlices);
    return (
        <>
            <div className={`article-block__item ${className}`}>
                <div className="article-block__item-img">
                    <img src={images} className="article-block__item-img-src"/>
                </div>
                <div className="article-block__item-text">
                    <div className="article-block__item-title">
                        <p className="text text-w700 text-s16">
                            {title}
                        </p>
                    </div>
                    {
                        original && (
                            <div className="article-block__item-title-original">
                                <p className="text text-s18">
                                    {titleOriginal}
                                </p>
                            </div>
                        )
                    }
                </div>
                <button className={'article-block__item-favorit'} onClick={async () => {
                    if (favorite.includes(id)) {
                        await deleteFavorit(id);
                    } else {
                        await addFavorit(id);
                    }
                }}>
                    <img className={'article-block__item-favorit-icon'}
                         src={favorite.includes(id) ? closeIcon : favoritIcon}/>
                </button>
            </div>
        </>
    )
});


export default Article;
