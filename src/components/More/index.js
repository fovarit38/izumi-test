import React from 'react';
import './style.scss';

import ArRightIcon from './assets/images/ar_right.png';

const More = (({name, onClick, disable}) => {
    return (
        <>
            <button className={'btn'} onClick={onClick}>
                <span className={'text text-s16 text-w700'}>
                    {name}
                </span>
                <img className={'btn__icon'}
                     src={ArRightIcon}/>
            </button>
        </>
    )
});


export default More;
