import axios from 'axios';
import {NotificationManager} from "react-notifications";

const {REACT_APP_API_PATH} = process.env;

async function request(request, token) {

    request = Object.assign({
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        method: 'GET',
        url: '',
    }, request);

    request.url = REACT_APP_API_PATH + request.url;

    return await axios(request).then((response) => {
        return response.data;
    }).catch(function (error) {
        NotificationManager.error(error.message, 'Error');
        return [];
    });
}

export const getAnime = async (variables) => {
    var query = `query (
          $page: Int = 1
          ${variables?.perPage ? '$perPage:Int = 20' : ''}
          $id: Int
          $id_in:[Int]
          $type: MediaType
          $search: String
          $source: MediaSource
          $season: MediaSeason
          $sort: [MediaSort] = [POPULARITY_DESC, SCORE_DESC]
        ) {
          Page(page: $page ${variables?.perPage ? ', perPage: $perPage' : ''}) {
            pageInfo {
              total
              perPage
              currentPage
              lastPage
              hasNextPage
            }
            media(
              id: $id
              id_in:$id_in
              type: $type
              season: $season
              source: $source
              search: $search
              sort: $sort
            ) {
              id
              title {
                userPreferred
                native
              }
              coverImage {
                extraLarge
                large
                color
              }
              bannerImage
              description
              type
            }
          }
        }
`;

    const req = (await request({
        url: '/',
        method: 'POST',
        data: {
            query: query,
            variables,
        }
    }));
    if (req?.data?.Page) {
        return req.data.Page;
    }
    return [];
}
